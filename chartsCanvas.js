var timer;


var randomScalingFactor = function () {
    return Math.round(Math.random() * 100)
};
var getCurrentTime = function (secondVar) {
    var d = new Date(); // for now
    var secondVar2 = secondVar ? secondVar : 0;
    var seconds = d.getSeconds() - secondVar2;

    var minutes = d.getMinutes();
    var hours = d.getHours();
    if (seconds < 0) {
        minutes -= 1;
        seconds = 60 + seconds;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    if (minutes < 0) {
        hours -= 1;
        minutes = 60 + minutes;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }

    return (hours + ":" + minutes + ":" + seconds);
};
var getData = function () {
    return [0, 0, 0, 0, 0, 0, 0, 0]; //[57, 54, 45, 67, 54, 59, 65, 76];
};

var dataWaterYear = {
    labels: ["October", "November", "December", "January", "February", "March", "April"],
    datasets: [
        {
            label: "Water T-Data Per Year",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [50, 54, 75, 63, 54, 42, 65]
                        }
                    ]
};
var dataWaterMonth = {
    labels: [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31],
    datasets: [
        {
            label: " ",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [57, 54, 45, 67, 54, 59, 65, 76, 49, 54, 65, 45, 65, 76, 78, 54]
                        }
                    ]
};
var dataWaterCurrent = {
    labels: [getCurrentTime(7), getCurrentTime(6), getCurrentTime(5), getCurrentTime(4), getCurrentTime(3), getCurrentTime(2), getCurrentTime(1), getCurrentTime()],
    datasets: [
        {
            label: "Water T-Data Per Month",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: getData()
                        }
                    ]
};
var dataHumiCurrent = {
    labels: [getCurrentTime(7), getCurrentTime(6), getCurrentTime(5), getCurrentTime(4), getCurrentTime(3), getCurrentTime(2), getCurrentTime(1), getCurrentTime()],
    datasets: [
        {
            label: " ",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: getData()
                        }
                    ]
};


Chart.types.Line.extend({
    name: "LineWithLine",
    draw: function () {
        Chart.types.Line.prototype.draw.apply(this, arguments);

        var lines = this.options.limitLines;

        for (var i = lines.length; --i >= 0;) {

            var xStart = Math.round(this.scale.xScalePaddingLeft);
            var linePositionY = this.scale.calculateY(lines[i].value);

            this.chart.ctx.fillStyle = lines[i].color ? lines[i].color : this.scale.textColor;
            this.chart.ctx.font = this.scale.font;
            this.chart.ctx.textAlign = "left";
            this.chart.ctx.textBaseline = "top";

            if (this.scale.showLabels && lines[i].label) {
                this.chart.ctx.fillText(lines[i].label, xStart + 5, linePositionY);
            }

            this.chart.ctx.lineWidth = this.scale.gridLineWidth;
            this.chart.ctx.strokeStyle = lines[i].color ? lines[i].color : this.scale.gridLineColor;

            if (this.scale.showHorizontalLines) {
                this.chart.ctx.beginPath();
                this.chart.ctx.moveTo(xStart, linePositionY);
                this.chart.ctx.lineTo(this.scale.width, linePositionY);
                this.chart.ctx.stroke();
                this.chart.ctx.closePath();
            }

            this.chart.ctx.lineWidth = this.lineWidth;
            this.chart.ctx.strokeStyle = this.lineColor;
            this.chart.ctx.beginPath();
            this.chart.ctx.moveTo(xStart - 5, linePositionY);
            this.chart.ctx.lineTo(xStart, linePositionY);
            this.chart.ctx.stroke();
            this.chart.ctx.closePath();
        }
    }
});

window.onload = function () {
    if (document.getElementById("canvas") !== null) {
        var ctx = document.getElementById("canvas").getContext("2d");
        var myNewChart = new Chart(ctx).Line(dataWaterMonth, {
            responsive: true
        });


        new Chart(ctx).LineWithLine(dataWaterMonth, {
            datasetFill: false,
            limitLines: [
                {
                    label: 'max',
                    value: 80,
                    color: 'rgba(255, 0, 0, .8)'
        },
                {
                    label: 'min',
                    value: 50,
                    color: 'rgba(0, 255, 255, .8)'
        }
    ],
        });
    }
    if (document.getElementById("canvas2") !== null) {
        var ctx2 = document.getElementById("canvas2").getContext("2d");
        var myNewChart2 = new Chart(ctx2).Line(dataWaterYear, {
            responsive: true
        });

        new Chart(ctx2).LineWithLine(dataWaterYear, {
            datasetFill: false,
            limitLines: [
                {
                    label: 'max',
                    value: 80,
                    color: 'rgba(255, 0, 0, .8)'
        },
                {
                    label: 'min',
                    value: 50,
                    color: 'rgba(0, 255, 255, .8)'
        }
    ],
        });
    }
    if (document.getElementById("canvas3") !== null) {
        var ctx3 = document.getElementById("canvas3").getContext("2d");
        var myNewChart3 = new Chart(ctx3).Line(dataWaterCurrent, {
            responsive: true,
            scaleOverride: true,
            scaleSteps: 10,
            scaleStepWidth: 10,
            scaleStartValue: 0
        });


    }
    if (document.getElementById("canvas4") !== null) {
        var ctx4 = document.getElementById("canvas4").getContext("2d");
        var myNewChart4 = new Chart(ctx4).Line(dataHumiCurrent, {
            responsive: true,
        });
        var ttd = document.getElementById("temperature_current");
        var htd = document.getElementById("humidity_current");


        timer = window.setInterval(function () {
            var randomTemp = Math.round(Math.random() * 100);
            myNewChart3.addData([randomTemp], getCurrentTime());
            myNewChart3.removeData();
            ttd.innerHTML = randomTemp + " C";

            var randomHumi = Math.round(Math.random() * 100);
            myNewChart4.addData([randomHumi], getCurrentTime());
            myNewChart4.removeData();
            htd.innerHTML = randomHumi + " %";

        }, 1000);

    }

};